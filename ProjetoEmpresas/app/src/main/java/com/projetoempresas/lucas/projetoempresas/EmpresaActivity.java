package com.projetoempresas.lucas.projetoempresas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


public class EmpresaActivity extends AppCompatActivity {

    // Atributos
    private ImageView botao_voltar;
    private ImageView logo_empresa;
    private TextView nome_empresa;
    private TextView texto_empresa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresa);

        // Identificar variáveis
        botao_voltar = (ImageView) findViewById(R.id.voltar_id);
        logo_empresa = (ImageView) findViewById(R.id.imagem_empresa_id);
        nome_empresa = (TextView) findViewById(R.id.nome_empresa_id);
        texto_empresa = (TextView) findViewById(R.id.texto_empresa_id);

        // Ao clicar no botao voltar:
        botao_voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Voltar à Home Activity
                Intent intent = new Intent(EmpresaActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

    }
}
