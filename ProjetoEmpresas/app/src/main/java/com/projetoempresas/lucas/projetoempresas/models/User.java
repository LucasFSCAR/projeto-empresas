package com.projetoempresas.lucas.projetoempresas.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas on 04/01/2018.
 */

public class User {

    // Atributos
    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String password;

    // Construtor padrão
    public User (String email, String password){
        this.email = email;
        this.password = password;
    }

    // Métodos getters e setters
    public String getEmail(){
        return this.email;
    }

    public String getPassword(){
        return this.password;
    }

    public void setEmail(String x){
        this.email = x;
    }

    public void setPassword(String x){
        this.password = x;
    }

}