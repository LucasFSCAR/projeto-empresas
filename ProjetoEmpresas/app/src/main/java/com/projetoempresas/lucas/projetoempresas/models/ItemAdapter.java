package com.projetoempresas.lucas.projetoempresas.models;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.projetoempresas.lucas.projetoempresas.R;


/**
 * Created by lucas on 07/01/2018.
 */

public class ItemAdapter extends ArrayAdapter{

    // Atributos do ItemAdapter
    private String[] nomeEmpresas;
    private String[] tipoEmpresas;
    private String[] paisEmpresas;
    private int[] imagemEmpresas;
    private Context context;

        // Construtor padrão
        public ItemAdapter(Context context, String[] nomeEmpresas1, String[] tipoEmpresas1, String[] paisEmpresas1, int[] imagemEmpresas1 ){
            // Sobrescrever o construtor do ArrayAdapter
            super(context, R.layout.item_layout, R.id.nome_empresa_id, nomeEmpresas1);
            this.context = context;
            this.nomeEmpresas = nomeEmpresas1;
            this.tipoEmpresas = tipoEmpresas1;
            this.paisEmpresas = paisEmpresas1;
            this.imagemEmpresas = imagemEmpresas1;
        }


    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Inflar o layout
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.item_layout, parent, false);

        // Referênciar objetos
        TextView nome = (TextView) row.findViewById(R.id.nome_empresa_id);
        TextView tipo = (TextView) row.findViewById(R.id.tipo_empresa_id);
        TextView pais = (TextView) row.findViewById(R.id.pais_empresa_id);
        ImageView imagem = (ImageView) row.findViewById(R.id.imagem_empresa_id);

        // Setar dados aos objetos
        nome.setText(nomeEmpresas[position]);
        tipo.setText(tipoEmpresas[position]);
        pais.setText(paisEmpresas[position]);
        //imagem.setImageResource(imagemEmpresas[position]);

        return row;
    }
}