package com.projetoempresas.lucas.projetoempresas;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.projetoempresas.lucas.projetoempresas.models.Empresa;
import com.projetoempresas.lucas.projetoempresas.models.ItemAdapter;

public class HomeActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    // Atributos
    private ImageView lupa1;
    private Toolbar toolbarDefault;
    private Toolbar toolbarPesquisa;
    private TextView textoAux;
    private SearchView pesquisa;
    private Context context = this;

    // Atributos do ListView
    private ListView lv;

    // Recuperar Dados
    String[] nomesEmpresa = {"Empresa1"};
    String[] tipoEmpresa = {"Negócio"};
    String[] paisEmpresa = {"Brasil"};
    int[] imagemEmpresa = {R.drawable.img_e_1_lista};

    // Método onCreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Identificar variáveis
        lupa1 = (ImageView) findViewById(R.id.ImageView_Icon_Search);
        toolbarDefault = (Toolbar) findViewById(R.id.toolbar_default);
        toolbarPesquisa = (Toolbar) findViewById(R.id.toolbar_pesquisa);
        textoAux = (TextView) findViewById(R.id.text_id);
        pesquisa = (SearchView) findViewById(R.id.SearchView_id);

        // ListView
        lv = (ListView) findViewById(R.id.listview_id);

        // Ao clicar na lupa de pesquisa:
        lupa1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Ocultar toolbar default e texto; e mostrar a toolbar de pesquisa
                toolbarDefault.setVisibility(View.GONE);
                textoAux.setVisibility(View.GONE);
                toolbarPesquisa.setVisibility(View.VISIBLE);

                // Focar na caixa de texto do SearchView e abrir o teclado
                pesquisa.setIconified(false);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(pesquisa, InputMethodManager.SHOW_IMPLICIT);

                // Definir adaptador que tratará os dados e montará os itens
                final ItemAdapter adapter = new ItemAdapter(context, nomesEmpresa, tipoEmpresa, paisEmpresa, imagemEmpresa);

                // Ao inserir uma pesquisa
                pesquisa.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    // Ao submeter a pesquisa
                    @Override
                    public boolean onQueryTextSubmit(String query) {

                        return false;
                    }

                    // Quando houver alterção no texto
                    @Override
                    public boolean onQueryTextChange(String newText) {
                        // Chamar metodo de busca
                        buscar(newText, adapter);
                        return true;
                    }
                });
            }
        });

        // Ao clicar em um item da ListView:
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Pegar nome do item clicado
                String item = (String) lv.getItemAtPosition(position);

                // Se for a empresa1, abrir acitivity de detalhes
                if ( item.equals("Empresa1") ) {
                    // Abrir activity de detalhes da empresa
                    Intent intent = new Intent(HomeActivity.this, EmpresaActivity.class);
                    startActivity(intent);
                }
            }
        });

    } // end onCreate


    // Método para realizar a busca da empresa
    public void buscar(String texto, ItemAdapter adapter){

        // Filtrar itens mostrados
        adapter.getFilter().filter(texto);
        // Setar adaptador no ListView
        lv.setAdapter(adapter);
    }

    // Sobreescrever métodos
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return true;
    }
}