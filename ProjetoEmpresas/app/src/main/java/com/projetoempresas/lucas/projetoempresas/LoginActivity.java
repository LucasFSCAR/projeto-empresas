package com.projetoempresas.lucas.projetoempresas;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.projetoempresas.lucas.projetoempresas.models.User;
import com.projetoempresas.lucas.projetoempresas.models.UserInterface;
import com.projetoempresas.lucas.projetoempresas.models.UserResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    // Atributos
    private Button button_entrar;
    private EditText edt_email;
    private EditText edt_senha;

    // Método onCreate da LoginActivity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Identificar variáveis
        edt_email = (EditText) findViewById(R.id.email_id);
        edt_senha = (EditText) findViewById(R.id.senha_id);
        button_entrar = (Button) findViewById(R.id.button_entrar_id);


        // Ao clicar no botão entrar:
        button_entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Verificar se as caixas de entradas estão vazias:
                    // Se a caixa de email estiver vazia
                if (edt_email.getText().toString().isEmpty()){
                    edt_email.setError("Campo de email vazio!");

                    // Se a caixa da senha estiver vazia
                } else if (edt_senha.getText().toString().isEmpty()){
                    edt_senha.setError("Campo de senha vazio!");

                    // Se não estiverem vazios:
                } else {
                    // Criar um user com o email e a senha dos editTexts
                    User user = new User(edt_email.getText().toString(), edt_senha.getText().toString());
                    // Chamar a função de login
                    login(user);
                }

            }

            // Método para realizar o login; dado usuário(email e senha)
            public void login(User user){
                // Validar login e senha

                // Retrofit
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://54.94.179.135:8090/api/v1/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                UserInterface service = retrofit.create(UserInterface.class);
                Call<UserResponse> call = service.login(user);
                call.enqueue(new Callback<UserResponse>() {

                    // Ao receber uma resposta
                    @Override
                    public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                        // Conferir resposta
                        // Se estiver certo
                        if ( response.isSuccessful() ){

                            // Salvar os 3 headers usando SharedPreferences
                            // Instanciar SharedPreferences
                            SharedPreferences sp = getSharedPreferences(UserInterface.ARQUIVO, 0);
                                SharedPreferences.Editor editor = sp.edit();

                                // Adicionar chaves e conteúdo
                                editor.putString("access-token",response.headers().get("access-token"));
                                editor.putString("client",response.headers().get("client"));
                                editor.putString("uid",response.headers().get("uid"));
                                // Submeter adições
                                editor.commit();

                            // Ir para a próxima acticity
                            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                            startActivity(intent);

                        // Se estiver errado
                        } else {
                            // Mostrar mensagem de erro
                            Toast.makeText(LoginActivity.this, "Usuário ou senha invalidos!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    // Ao falhar
                    @Override
                    public void onFailure(Call<UserResponse> call, Throwable t) {
                        // Mostrar mensagem de erro
                        Toast.makeText(LoginActivity.this, "ERRO DE CONEXÃO!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}