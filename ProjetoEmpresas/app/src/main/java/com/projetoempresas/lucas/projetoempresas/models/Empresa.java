package com.projetoempresas.lucas.projetoempresas.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas on 06/01/2018.
 */

public class Empresa {

    // Atributos
    @SerializedName("name")
    private String name;
    @SerializedName("details")
    private String details;
    @SerializedName("country")
    private String country;
    @SerializedName("type")
    private String type;
    @SerializedName("image")
    private int image;

    // Construtor padrão
    public Empresa(String name, String details, String country, String type, int image){
        this.name = name;
        this.details = details;
        this.country = country;
        this.type = type;
        this.image = image;
    }

    // Métodos getters e setters
    public void setName(String name){
        this.name = name;
    }

    public void setDetails(String details){
        this.details = details;
    }

    public void setCountry(String country){
        this.country = country;
    }

    public void setType(String type){
        this.type = type;
    }

    public void setImage(int image){
        this.image = image;
    }

    public int getImage(){
        return this.image;
    }

    public String getType(){
        return this.type;
    }

    public String getName(){
        return this.name;
    }

    public String getCountry(){
        return this.country;
    }

    public String getDetails(){
        return this.details;
    }

}